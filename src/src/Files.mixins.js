import axios from 'axios'

export const baseUrl = '//download.omarea.com/scene7/'

export default {
  methods: {
    loadFiles () {
      const _this = this;
      axios.default.get(baseUrl + this.dir).then(function (r) {
        if (r.data) {
          /*
            const parser = new DOMParser()
            const dom = parser.parseFromString(r.data, 'text/html')
            const links = dom.getElementsByTagName('a')
            this.links = links
          */
          _this.parseDom(r.data);
        }
      });
    },
    jumpDownload (link) {
      const dir = link.getAttribute('href')
      if (dir.indexOf('.') < 0) {
        this.$router.push({
          path: '/versions',
          query: { dir: dir }
        })
      } else {
        const parent = this.dir ? this.dir : '/'
        const fullUrl = baseUrl + parent + dir;
        if (fullUrl.endsWith('.md')) {
          this.$router.push({
            path: '/markdown',
            query: { doc: fullUrl }
          })
        } else {
          window.location.href = fullUrl;
        }
      }
    },
    parseDom (xml) {
      var parser = new DOMParser();
      var links = xml.split('\n').filter(function (it) {
        return it.indexOf('<a') == 0;
      }).map(function (it) {
        try {
          // ['<a', 'href="scene_7.0.0.apk">scene_7.0.0.apk</a>', '02-Jan-2024', '01:54', '7922978']
          var columns = it.split(/[ ]+/).filter(function (it) {
            return !!it;
          });
          var link = parser.parseFromString(it, 'text/html').getElementsByTagName('a')[0];

          if (columns.length > 4) {
            var date = new Date(columns[columns.length - 3] + ' ' + columns[columns.length - 2]);
            link.date = date.toLocaleString();
            var size = parseInt((parseInt(columns[columns.length - 1]) || 0) / 1000);
            link.time = size < 100 ? Date.now() : date.getTime();
            link.size = '' + size + 'KB';
          }

          return link;
        } catch (ex) {
          console.error(it);
        }
      }).filter(function (it) {
        return it;
      });
      if (!this.dir.includes('doc')) {
        links.sort(function (a, b) {
          return b.time - a.time;
        });
      }
      this.links = links;
    }
  },
  computed: {
    dir () {
      return this.$route.query.dir || '';
    }
  }
}