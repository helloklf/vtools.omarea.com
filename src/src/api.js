import axios from 'axios'
import { encode } from 'js-base64'
import CryptoJS from 'crypto-js'

axios.defaults.maxRedirects = 0

const jsonToString = (json) => {
  return encode(JSON.stringify(json), true)
}

const cloudHost = location.protocol === 'https:' ? "https://vtools-api.omarea.com" : "http://vtools-api.omarea.com"

export const sendEmail = (email) => {
  return axios.post(cloudHost + '/account-send-token', jsonToString({
    uid: email,
  }))
}

export const resetPwd = (email, token, password) => {
  return axios.post(cloudHost + '/account-reset-pwd', jsonToString({
    uid: email,
    token,
    password: CryptoJS.MD5(password).toString(),
  }))
}

export const modifyPwd = (email, oldPwd, newPassword) => {
  return axios.post(cloudHost + '/account-modify-pwd', jsonToString({
    uid: email,
    old: CryptoJS.MD5(oldPwd).toString(),
    new: CryptoJS.MD5(newPassword).toString(),
  }))
}

export const accountRecord = (email, password) => {
  return axios.post(cloudHost + '/account-integral', jsonToString({
    uid: email,
    password: CryptoJS.MD5(password).toString(),
  }))
}

export const deleteAccount = (email, token, password) => {
  return axios.post(cloudHost + '/account-delete', jsonToString({
    uid: email,
    token,
    password: CryptoJS.MD5(password).toString(),
  }))
}


export const modifyAccount = (email, token, password) => {
  return axios.post(cloudHost + '/account-modify-uid', jsonToString({
    token,
    uid: email,
    password: CryptoJS.MD5(password).toString(),
  }))
}
