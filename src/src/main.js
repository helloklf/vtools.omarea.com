import Vue from 'vue'
import VueRouter from 'vue-router'
import './styles/App.scss'
import App from './App.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    { path: '/versions', component: () => import('./Versions.vue') },
    { path: '/user', component: () => import('./User.vue') },
    { path: '/pwd', component: () => import('./user/Pwd.vue') },
    { path: '/record', component: () => import('./user/Record.vue') },
    { path: '/email', component: () => import('./user/Email.vue') },
    { path: '/user-name', component: () => import('./user/UserName.vue') },
    { path: '/faq', component: () => import('./user/Faq.vue') },
    { path: '/devices', component: () => import('./user/Devices.vue') },
    { path: '/destroy', component: () => import('./user/Destroy.vue') },
    { path: '/markdown', component: () => import('./Markdown.vue') },
    { path: '/*', component: () => import('./Index.vue') },
  ]
})

new Vue({
  el: '#app',
  router,
  render (h) {
    return h(App)
  }
})